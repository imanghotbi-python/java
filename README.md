# Very simple Java example


Download `junit-4.12.jar` and `hamcrest-core-1.3.jar` before proceeding.

To compile tests locally:

```
javac -cp '.:junit-4.12.jar:hamcrest-core-1.3.jar' ./CalculatorTest.java
```

To run tests locally:

```
java -cp '.:junit-4.12.jar:hamcrest-core-1.3.jar' org.junit.runner.JUnitCore CalculatorTest
``
change for test

